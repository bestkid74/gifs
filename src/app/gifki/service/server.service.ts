import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class ServerService {
    constructor(private http: HttpClient) {}

    getRandomGifs(counter: number = 10) {
       return this.http.get(`https://api.giphy.com/v1/gifs/trending?api_key=5DGj7TMOx7BxTpLF0a7ZBKVMsdPg0Q3v&limit=${counter}&rating=G`);
    }

    getSearchGifs(query: string = '', counter: number = 5) {
       return this.http
           .get(`https://api.giphy.com/v1/gifs/search?api_key=5DGj7TMOx7BxTpLF0a7ZBKVMsdPg0Q3v&q=${query}&limit=${counter}&offset=0&rating=G&lang=en`);
    }
}