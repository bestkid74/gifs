import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';

import {ServerService} from './service/server.service';


@Component({
  selector: 'app-gifki',
  templateUrl: './gifki.component.html',
  styleUrls: ['./gifki.component.sass']
})
export class GifkiComponent implements OnInit, OnDestroy {
    sub1: Subscription;
    sub2: Subscription;
    sub3: Subscription;
    sub4: Subscription;
    gifs = [];
    loading = false;
    values = 'not found';

  constructor(private server: ServerService) { }

  ngOnInit() {
      this.loading = true;
    this.sub1 = this.server.getRandomGifs().subscribe((response) => {

      response['data'].forEach((item) => {
          this.gifs.push({
              source: item['images']['original']['url']
          });
          this.loading = false;
      });

    });
  }

    onKey(value: string) {
        this.values = value;
        this.loading = true;
        this.sub2 = this.server.getSearchGifs(this.values, 15).subscribe((response) => {
            this.gifs = [];
            if (response['data'] && response['data'].length > 0) {
                setTimeout(() => {
                    response['data'].forEach((item) => {
                        this.gifs.push({
                            source: item['images']['original']['url']
                        });
                    });
                    this.loading = false;
                }, 1500);
            } else {
                this.sub4 = this.server.getSearchGifs('not found', 1).subscribe((resp) => {
                    setTimeout(() => {
                        resp['data'].forEach((item) => {
                            this.gifs.push({
                                source: item['images']['original']['url']
                            });
                        });
                        this.loading = false;
                    }, 1500);
                });
            }
        });
    }

    onScroll() {
        this.loading = true;
        this.sub3 = this.server.getSearchGifs(this.values).subscribe((response) => {

            response['data'].forEach((item) => {
                setTimeout(() => {
                    this.gifs.push({
                        source: item['images']['original']['url']
                    });
                    this.loading = false;
                }, 1500);
            });

        });
    }

    ngOnDestroy() {
        this.sub1.unsubscribe();
        this.sub2.unsubscribe();
        this.sub3.unsubscribe();
        this.sub4.unsubscribe();
    }
}
