import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {HttpClientModule} from '@angular/common/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { GifkiComponent } from './gifki/gifki.component';
import {ServerService} from './gifki/service/server.service';
import {SpinnerModule} from 'spinner-angular';


@NgModule({
  declarations: [
    AppComponent,
    GifkiComponent
  ],
  imports: [
      BrowserModule,
      BrowserAnimationsModule,
      MatToolbarModule,
      MatIconModule,
      MatInputModule,
      HttpClientModule,
      InfiniteScrollModule,
      FormsModule,
      SpinnerModule.forRoot({})
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule {}
